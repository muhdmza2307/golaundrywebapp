﻿using goLaundryWebApp.Common;
using goLaundryWebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _UserLogger;
        private readonly ILogger<GridViewController> _GridLogger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;
        private readonly string compInfoViewHtml = @"~/Views/Company/CompanyInfo.cshtml";

        public UserController(IHttpContextAccessor httpContextAccessor, ILogger<UserController> logger, ILogger<GridViewController> gridlogger)
        {
            _UserLogger = logger;
            _GridLogger = gridlogger;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }

        public IActionResult UserList()
        {
            GridViewController objGridViewCtrl = new GridViewController(_httpContextAccessor, _GridLogger);
            ListGridView objListGridView = new ListGridView();
            objListGridView.GridView = new List<GridView>();
            objListGridView.GridView.Add(new GridView("dtGridUserList"));

            objListGridView.GridView[0].ColumnHide = new int[] { 0 };
            objListGridView.GridView[0].ColumnNoSort = new int[] { };
            objListGridView.GridView[0].IsCheckBoxRequired = "N";
            objListGridView.GridView[0].ColumnWidth = new string[] { "", "20%", "20%", "10%", "10%", "10%", "10%", "10%", "" };
            objListGridView.GridView[0].GridViewModuleName = "Administration";
            objListGridView.GridView[0].GridViewSubModuleName = "UserList";
            objListGridView.GridView[0].GridViewApiCall = APIUrlCallVar.api_UserGridList;
            objListGridView.GridView[0].actionSetup = "btnEdit,Edit,fas fa-edit,#|btnView,View,fas fa-eye,#";
            //objListGridView.GridView[0].editValue1 = "iCOID";
            clsSession.setSession("sessionGridView", JsonConvert.SerializeObject(objListGridView));

            objGridViewCtrl.GridViewListingReturn();

            return View();
        }
    }
}
