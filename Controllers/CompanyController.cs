﻿using goLaundryWebApp.Common;
using goLaundryWebApp.Models;
using goLaundryWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ILogger<CompanyController> _CompLogger;
        private readonly ILogger<GridViewController> _GridLogger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;
        private readonly string compInfoViewHtml = @"~/Views/Company/CompanyInfo.cshtml";

        public CompanyController(IHttpContextAccessor httpContextAccessor, ILogger<CompanyController> logger, ILogger<GridViewController> gridlogger)
        {
            _CompLogger = logger;
            _GridLogger = gridlogger;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }


        public IActionResult AddCompany()
        {
            CompanyModel objComp = new CompanyModel();

            TempData["pageMode"] = ConstantVar.PageModeAdd;

            return View(compInfoViewHtml, objComp);
        }

        public IActionResult EditCompany(int CompId)
        {

            CompanyModel objComp = new CompanyModel();

            TempData["pageMode"] = ConstantVar.PageModeEdit;
            ViewData["Title"] = "Edit Company";

            string apiCall = APIUrlCallVar.api_ViewEditCompany;
            apiCall = apiCall.Replace("$", CompId.ToString());

            Task<ResponseModel> respReturn = new CommonAPI().APICallGetMethod(apiCall);
            respReturn.Wait();
            ResponseModel resp = respReturn.Result;
            DataSet dsData = CommonAPI.dataJsonToDataSet(resp.data);

            DataTable dtCompDetails = dsData.Tables[0];
            if (dtCompDetails.Rows.Count > 0)
            {
                objComp.compId = Convert.ToInt32(dtCompDetails.Rows[0]["iCOID"]);
                objComp.compName = dtCompDetails.Rows[0]["vaCONAME"].ToString();
                objComp.branchName = dtCompDetails.Rows[0]["vaBRANCH"].ToString();
                objComp.email = dtCompDetails.Rows[0]["vaEMAIL"].ToString();
                objComp.telNo = dtCompDetails.Rows[0]["vaTELNO"].ToString();
                objComp.faxNo = dtCompDetails.Rows[0]["vaFAXNO"].ToString();
                objComp.add1 = dtCompDetails.Rows[0]["vaADD1"].ToString();
                objComp.add2 = dtCompDetails.Rows[0]["vaADD2"].ToString();
                objComp.add3 = dtCompDetails.Rows[0]["vaADD3"].ToString();
                objComp.isActive = Convert.ToBoolean(dtCompDetails.Rows[0]["siSTATUS"].ToString());
            }


            return View(compInfoViewHtml, objComp);
        }

        public IActionResult ViewCompany(int CompId)
       {
            CompanyModel objComp = new CompanyModel();
            TempData["pageMode"] = ConstantVar.PageModeView;
            ViewData["Title"] = "View Company";

            string apiCall = APIUrlCallVar.api_ViewEditCompany;
            apiCall = apiCall.Replace("$", CompId.ToString());

            Task<ResponseModel> respReturn = new CommonAPI().APICallGetMethod(apiCall);
            respReturn.Wait();
            ResponseModel resp = respReturn.Result;
            DataSet dsData = CommonAPI.dataJsonToDataSet(resp.data);

            DataTable dtCompDetails = dsData.Tables[0];
            if (dtCompDetails.Rows.Count > 0)
            {
                objComp.compId = Convert.ToInt32(dtCompDetails.Rows[0]["iCOID"]);
                objComp.compName = dtCompDetails.Rows[0]["vaCONAME"].ToString();
                objComp.branchName = dtCompDetails.Rows[0]["vaBRANCH"].ToString();
                objComp.email = dtCompDetails.Rows[0]["vaEMAIL"].ToString();
                objComp.telNo = dtCompDetails.Rows[0]["vaTELNO"].ToString();
                objComp.faxNo = dtCompDetails.Rows[0]["vaFAXNO"].ToString();
                objComp.add1 = dtCompDetails.Rows[0]["vaADD1"].ToString();
                objComp.add2 = dtCompDetails.Rows[0]["vaADD2"].ToString();
                objComp.add3 = dtCompDetails.Rows[0]["vaADD3"].ToString();
                objComp.strIsActive = CommonFunction.convertStatusToStr(Convert.ToBoolean(dtCompDetails.Rows[0]["siSTATUS"].ToString()));
            }


            #region gridSetup
            GridViewController objGridViewCtrl = new GridViewController(_httpContextAccessor, _GridLogger);
            ListGridView objListGridView = new ListGridView();
            objListGridView.GridView = new List<GridView>();
            objListGridView.GridView.Add(new GridView("dtGridChildCompList"));

            objListGridView.GridView[0].ColumnHide = new int[] { 0 };
            objListGridView.GridView[0].ColumnNoSort = new int[] { };
            objListGridView.GridView[0].IsCheckBoxRequired = "N";
            objListGridView.GridView[0].ColumnWidth = new string[] { "", "55%", "35%" };
            objListGridView.GridView[0].GridViewModuleName = "Administration";
            objListGridView.GridView[0].GridViewSubModuleName = "CompanyChildList";
            objListGridView.GridView[0].dtGridView = dsData.Tables[1];
            objListGridView.GridView[0].actionSetup = "btnView,View,fas fa-eye,#";

            objListGridView.GridView.Add(new GridView("dtUserCompList"));
      
            objListGridView.GridView[1].ColumnHide = new int[] { 0 , 1};
            objListGridView.GridView[1].ColumnNoSort = new int[] { };
            objListGridView.GridView[1].IsCheckBoxRequired = "N";
            objListGridView.GridView[1].ColumnWidth = new string[] { "", "", "55%", "35%" };
            objListGridView.GridView[1].GridViewModuleName = "Administration";
            objListGridView.GridView[1].GridViewSubModuleName = "CompanyUserList";
            objListGridView.GridView[1].actionSetup = "btnView,View,fas fa-eye,#";
            objListGridView.GridView[1].dtGridView = dsData.Tables[2];

            clsSession.setSession("sessionGridView", JsonConvert.SerializeObject(objListGridView));

            objGridViewCtrl.GridViewListingReturn();

            #endregion

            return View(compInfoViewHtml, objComp);
        }

        public IActionResult CompanyList()
        {
            //string menuSessStr = clsSession.getSession("sessionUserMenuPerm");
            //DataTable objSessionUserMenuPerm = JsonConvert.DeserializeObject<DataTable>(menuSessStr);

            GridViewController objGridViewCtrl = new GridViewController(_httpContextAccessor, _GridLogger);
            ListGridView objListGridView = new ListGridView();
            objListGridView.GridView = new List<GridView>();
            objListGridView.GridView.Add(new GridView("dtGridCompList"));

            objListGridView.GridView[0].ColumnHide = new int[] { 0 };
            objListGridView.GridView[0].ColumnNoSort = new int[] {  };
            objListGridView.GridView[0].IsCheckBoxRequired = "N";
            objListGridView.GridView[0].ColumnWidth = new string[] { "", "40%", "12.5%", "12.5%", "12.5%", "12.5%", "" };
            objListGridView.GridView[0].GridViewModuleName = "Administration";
            objListGridView.GridView[0].GridViewSubModuleName = "CompanyList";
            objListGridView.GridView[0].GridViewApiCall = APIUrlCallVar.api_CompanyGridList;
            objListGridView.GridView[0].actionSetup = "btnEdit,Edit,fas fa-edit,#|btnView,View,fas fa-eye,#";
            //objListGridView.GridView[0].editValue1 = "iCOID";
            clsSession.setSession("sessionGridView", JsonConvert.SerializeObject(objListGridView));

            objGridViewCtrl.GridViewListingReturn();

            return View();
        }
    }
}
