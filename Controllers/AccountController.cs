﻿using goLaundryWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using goLaundryWebApp.Common;
using Newtonsoft.Json;
using System.Data;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace goLaundryWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _AccLogger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;
        private readonly string compInfoViewHtml = @"~/Views/Company/CompanyInfo.cshtml";

        public AccountController(IHttpContextAccessor httpContextAccessor, ILogger<AccountController> logger)
        {
            _AccLogger = logger;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }

        public IActionResult LogIn()
        {
            InitApi.InitializeClient("");
            return View();
        }

        [HttpPost]
        public IActionResult LogIn(AccountModel objAcc)
        {
            //InitApi.InitializeClient("");
            string jsonPassIn = JsonConvert.SerializeObject(objAcc);
            string apiCall = APIUrlCallVar.api_Login;
            Task<ResponseModel> respReturn = new CommonAPI().APICallPostMethod(apiCall, jsonPassIn);
            respReturn.Wait();
            ResponseModel resp = respReturn.Result;
           

            if (resp.type == 1)
            {
                DataTable dtData = CommonAPI.dataJsonToDataTable(resp.data);
                InitApi.InitializeClient(dtData.Rows[0]["token"].ToString());
                DataSet dsDataMenuPerm = setMenuAndPermissionUser(Convert.ToInt32(dtData.Rows[0]["iUSID"].ToString()));
                HttpContext.Session.SetString("sessionUserMenuPerm", JsonConvert.SerializeObject(dsDataMenuPerm));
                return RedirectToAction("CompanyList", "Company");
            }
            else
            {
                string msg = resp.data;
                PartialViewModel pm = new PartialViewModel();
                pm.Message = msg;
                var viewX = this.RenderViewAsync("Partial", "_modelPopUp", pm, true);
                viewX.Wait();
            }

            return View();
        }

        public DataSet setMenuAndPermissionUser(int userId)
        {
            DataSet dsDataMenuPerm = new DataSet();

            string apiCall = APIUrlCallVar.api_MenuPerm;
            apiCall = apiCall.Replace("$", userId.ToString());
            Task<ResponseModel> respReturn = new CommonAPI().APICallGetMethod(apiCall);
            respReturn.Wait();
            ResponseModel resp = respReturn.Result;
            dsDataMenuPerm = CommonAPI.dataJsonToDataSet(resp.data);

            return dsDataMenuPerm;
        }

        
    }
}
