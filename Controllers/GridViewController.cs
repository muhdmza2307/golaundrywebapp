﻿using goLaundryWebApp.Common;
using goLaundryWebApp.Models;
using goLaundryWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Controllers
{
    public class GridViewController : Controller
    {
        private readonly ILogger<GridViewController> _GridLogger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;
        string gridSessStr = "";

        public GridViewController(IHttpContextAccessor httpContextAccessor, ILogger<GridViewController> logger)
        {
            _GridLogger = logger;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }

        public void GridViewListingReturn()
        {
            try
            {
                gridSessStr = clsSession.getSession("sessionGridView");
                ListGridView objSessionGrid = JsonConvert.DeserializeObject<ListGridView>(gridSessStr);
                for (int i = 0; i < objSessionGrid.GridView.Count; i++)
                {
                    var j = MultiGetList(objSessionGrid.GridView[i].GridViewID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IActionResult MultiGetList(string GridViewID)
        {
            gridSessStr = clsSession.getSession("sessionGridView");
            ListGridView objListGridView = JsonConvert.DeserializeObject<ListGridView>(gridSessStr);


            for (int i = 0; i < objListGridView.GridView.Count; i++)
            {
                if (objListGridView.GridView[i].GridViewID == GridViewID && objListGridView.GridView[i].GridViewApiCall != "")
                {
                    Task<ResponseModel> respReturn = new CommonAPI().APICallGetMethod(objListGridView.GridView[i].GridViewApiCall.ToString());
                    respReturn.Wait();
                    ResponseModel resp = respReturn.Result;

                    DataTable dtResult = CommonAPI.dataJsonToDataTable(resp.data);

                    if (dtResult != null && dtResult.Rows.Count > 0)
                    {
                        objListGridView.GridView[i].dtGridView = dtResult;
                    }
                }

            }

            clsSession.setSession("sessionGridView", JsonConvert.SerializeObject(objListGridView));
            return PartialView(@"~/Views/Shared/Components/GridPartial/GridView.cshtml", objListGridView);
        }
    }
}
