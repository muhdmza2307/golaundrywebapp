#pragma checksum "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c43035daf5817066d53703e90d02e9f7dd24cc49"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Company_CompanyInfo), @"mvc.1.0.view", @"/Views/Company/CompanyInfo.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\_ViewImports.cshtml"
using goLaundryWebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
using goLaundryWebApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c43035daf5817066d53703e90d02e9f7dd24cc49", @"/Views/Company/CompanyInfo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"40a60171360077f065337961835c49748508a053", @"/Views/_ViewImports.cshtml")]
    public class Views_Company_CompanyInfo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<goLaundryWebApp.Models.CompanyModel>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
  
    int pageMode = (int)TempData["pageMode"];
    int modeView = ConstantVar.PageModeView;
    string[] optionListIsActive = new string[] { "InActive", "Active" };

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"contentAndNav\">\r\n    <div class=\"panel\">\r\n        <h2 class=\"color-primary-04 bg-primary-03\">");
#nullable restore
#line 11 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                                              Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</h2>
        <div class=""mod--theme-secondary"">
            <div class=""row"">
                <div class=""col-lg-6 col-md-6 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-2 col-md-2 col-xs-12 col-sm-12 textfield"">
                            <div>
                                Company Name<span class=""asterisk"">*</span>
                            </div>
                        </div>

                        <div class=""col-lg-10 col-md-10 col-xs-12 col-sm-12 textfield"">
                            <input type=""hidden"" id=""compId"" name=""compId""");
            BeginWriteAttribute("value", " value=\"", 1006, "\"", 1027, 1);
#nullable restore
#line 23 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 1014, Model.compId, 1014, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                            <input type=\"text\" id=\"compName\" name=\"compName\" Class=\"form-control\"");
            BeginWriteAttribute("value", " value=\"", 1130, "\"", 1153, 1);
#nullable restore
#line 24 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 1138, Model.compName, 1138, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>

                    </div>
                </div>
                <div class=""col-lg-6 col-md-6 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-2 col-md-2 col-xs-12 col-sm-12 textfield"">
                            <div>
                                Branch<span class=""asterisk"">*</span>
                            </div>
                        </div>
                        <div class=""col-lg-10 col-md-10 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""branchName"" name=""branchName"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 1814, "\"", 1839, 1);
#nullable restore
#line 37 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 1822, Model.branchName, 1822, 17, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                    </div>

                </div>
            </div>

            <div class=""row"">
                <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Email<span class=""asterisk"">*</span>
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""email"" name=""email"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 2467, "\"", 2487, 1);
#nullable restore
#line 51 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 2475, Model.email, 2475, 12, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Telephone No<span class=""asterisk"">*</span>
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""telNo"" name=""telNo"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 2895, "\"", 2915, 1);
#nullable restore
#line 57 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 2903, Model.telNo, 2903, 12, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                    </div>
                </div>
            </div>

            <div class=""row"">
                <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Fax No<span class=""asterisk"">*</span>
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""faxNo"" name=""faxNo"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 3542, "\"", 3562, 1);
#nullable restore
#line 70 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 3550, Model.faxNo, 3550, 12, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Address 1<span class=""asterisk"">*</span>
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""add1"" name=""add1"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 3965, "\"", 3984, 1);
#nullable restore
#line 76 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 3973, Model.add1, 3973, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                    </div>
                </div>
            </div>

            <div class=""row"">
                <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Address 2
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""add2"" name=""add2"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 4581, "\"", 4600, 1);
#nullable restore
#line 89 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 4589, Model.add2, 4589, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Address 3
                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">
                            <input type=""text"" id=""add3"" name=""add3"" Class=""form-control""");
            BeginWriteAttribute("value", " value=\"", 4972, "\"", 4991, 1);
#nullable restore
#line 95 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 4980, Model.add3, 4980, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                    </div>
                </div>
            </div>

            <div class=""row"">
                <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">
                            Status<span class=""asterisk"">*</span>
                        </div>
");
            WriteLiteral("                        <div class=\"col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield\">\r\n                             <select id=\"isActive\" name=\"isActive\" class=\"form-control js-select mandatory\"");
            BeginWriteAttribute("value", " value=\"", 6376, "\"", 6416, 1);
#nullable restore
#line 123 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
WriteAttributeValue("", 6384, Convert.ToInt32(Model.isActive), 6384, 32, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n");
#nullable restore
#line 124 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                                 for (int j = 0; j < optionListIsActive.Length; j++)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c43035daf5817066d53703e90d02e9f7dd24cc4913512", async() => {
#nullable restore
#line 126 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                                                                                                                        Write(optionListIsActive[j].ToString());

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "selected", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 126 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
AddHtmlAttributeValue("", 6595, Convert.ToInt32(Model.isActive)== j ? "selected" : null, 6595, 58, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 126 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                                                                                                             WriteLiteral(j);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 127 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                            </select>
                            
                        </div>
                        <div class=""col-lg-1 col-md-1 col-xs-12 col-sm-12 textfield"">

                        </div>
                        <div class=""col-lg-5 col-md-5 col-xs-12 col-sm-12 textfield"">

                        </div>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <div class=""row"">
                <div class=""col-md-12 col-lg-12 col-xs-12 col-sm-12 textfield"">
                    <div class=""row"">
                        <div class=""col-md-6 col-lg-6 col-xs-12 col-sm-12 btnfield"">
                            <input type=""button"" id=""btnBack"" name=""btnBack"" value=""<< Go To List"" Class=""btn btn--secondary text-center""");
            BeginWriteAttribute("onclick", " onclick=\"", 7576, "\"", 7586, 0);
            EndWriteAttribute();
            WriteLiteral(@" />
                        </div>
                        <div class=""col-md-6 col-lg-6 col-xs-12 col-sm-12 btnfield"">
                            <input type=""button"" id=""btnCreate"" name=""btnCreate"" value=""Create >>"" Class=""btn btn--primary text-center""");
            BeginWriteAttribute("onclick", " onclick=\"", 7845, "\"", 7855, 0);
            EndWriteAttribute();
            WriteLiteral(" />\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
#nullable restore
#line 159 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
 if (pageMode == modeView)
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <div class=""contentAndNav"">
        <div class=""panel"">
            <h2 class=""color-primary-04 bg-primary-03"">Company Details</h2>
            <div class=""mod--theme-secondary"">
                <div class=""row"">
                    <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                        <div class=""reminder"">
                            Sub Company List
                        </div>
                    </div>
                </div>

                <div class=""row"">
                    <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12"">
                        ");
#nullable restore
#line 175 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                   Write(await Component.InvokeAsync("GridPartial", new { GridViewNo = 0 }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                    </div>
                </div>
                <br />
                <br />

                <div class=""row"">
                    <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12 textfield"">
                        <div class=""reminder"">
                            User List
                        </div>
                    </div>
                </div>

                <div class=""row"">
                    <div class=""col-lg-12 col-md-12 col-xs-12 col-sm-12"">
                        ");
#nullable restore
#line 191 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                   Write(await Component.InvokeAsync("GridPartial", new { GridViewNo = 1 }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n");
#nullable restore
#line 198 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"

}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script>\r\n        var pageMode = \'");
#nullable restore
#line 203 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                   Write(pageMode);

#line default
#line hidden
#nullable disable
                WriteLiteral("\';\r\n        var modeView = \'");
#nullable restore
#line 204 "C:\Users\Muhammad Mirza\Documents\dev\goLaundryWebApp\goLaundryWebApp\Views\Company\CompanyInfo.cshtml"
                   Write(ConstantVar.PageModeView);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"';

        $(window).on( ""load"", function () {
            if (pageMode == modeView) {
                $('.asterisk').hide();
                $('.textfield > .form-control').prop('disabled', true);
                $('.btnfield > .btn').hide();
            }
        });
    </script>
");
            }
            );
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<goLaundryWebApp.Models.CompanyModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
