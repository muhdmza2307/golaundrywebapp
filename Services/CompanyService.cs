﻿using goLaundryWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using goLaundryWebApp.Common;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Linq;

namespace goLaundryWebApp.Services
{
    public class CompanyService
    {
        private string uri;
        private ResponseModel res = new ResponseModel();

        public async Task<string> GetCompanyDetails(int compId)
        {
            uri = "api/Company/GetCompanyById?Id=" + compId.ToString();
            string json = "";

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.GetAsync(uri);
                json = await result.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {

            }

            return json;
        }

        public async Task<ResponseModel> GetCompanyDetails2(int compId)
        {
            uri = "api/Company/GetCompanyById?Id=" + compId.ToString();
            string json = "";

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.GetAsync(uri);
                json = await result.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ResponseModel>(json);

            }
            catch (Exception ex)
            {

            }

            return res;
        }
    }
}
