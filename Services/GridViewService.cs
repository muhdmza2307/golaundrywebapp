﻿using goLaundryWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using goLaundryWebApp.Common;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Linq;

namespace goLaundryWebApp.Services
{
    public class GridViewService
    {
        private string uri;
        private ResponseModel res = new ResponseModel();

        public async Task<string> GetGridList(string ApiCall)
        {
            uri = "api/" + ApiCall;
            string json = "";

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.GetAsync(uri);
                json = await result.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {

            }

            return json;
        }

        public async Task<ResponseModel> GetGridList2(string ApiCall)
        {
            uri = "api/" + ApiCall;
            string json = "";

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.GetAsync(uri);
                json = await result.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ResponseModel>(json);

            }
            catch (Exception ex)
            {

            }

            return res;
        }
    }
}
