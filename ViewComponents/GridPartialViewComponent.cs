﻿using goLaundryWebApp.Common;
using goLaundryWebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.ViewComponents
{
    public class GridPartialViewComponent : ViewComponent
    {
        private readonly ILogger<GridPartialViewComponent> _GridLogger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;
        string gridSessStr = "";

        public GridPartialViewComponent(IHttpContextAccessor httpContextAccessor, ILogger<GridPartialViewComponent> logger)
        {
            _GridLogger = logger;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }

        public async Task<IViewComponentResult> InvokeAsync(int GridViewNo)
        {
            ListGridView objListGridView = new ListGridView();
            gridSessStr = clsSession.getSession("sessionGridView");
            objListGridView = JsonConvert.DeserializeObject<ListGridView>(gridSessStr);
            objListGridView.GridviewNo = GridViewNo;
            //objListGridView = HttpContext.Session.GetObjectFromJson<ListGridView>("sessionGridView");

            return View(objListGridView);
        }
    }
}
