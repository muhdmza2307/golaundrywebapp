﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace goLaundryWebApp.Common
{
    public class CommonFunction
    {
        public static string convertStatusToStr(bool input)
        {
            string output = "";

            if (input == true)
                output = "Active";
            else if (input == false)
                output = "Inactive";

            return output;
        }
    }
}
