﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Common
{
    public class Session
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public Session(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void setSession(string key, string value)
        {
            _session.SetString(key, value);
        }

        public string getSession(string key)
        {
            string value = _session.GetString(key);
            return value;
    }
    }
}
