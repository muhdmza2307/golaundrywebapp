﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace goLaundryWebApp.Common
{
    public class InitApi
    {
        public static HttpClient apiClient { get; set; }

        public static void InitializeClient(string token)
        {
            apiClient = new HttpClient();
            apiClient.BaseAddress = new Uri("http://localhost:60483/");
            apiClient.DefaultRequestHeaders.Accept.Clear();
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
    }
}
