﻿using goLaundryWebApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace goLaundryWebApp.Common
{
    public class CommonAPI
    {
        public static DataSet dataJsonToDataSet(string dataInp)
        {
            DataSet dsReturn = new DataSet();
            dsReturn = JsonConvert.DeserializeObject<DataSet>(dataInp);
            return dsReturn;
        }

        public static DataTable dataJsonToDataTable(string dataInp)
        {
            DataTable dtReturn = new DataTable();
            dtReturn = JsonConvert.DeserializeObject<DataTable>(dataInp);
            return dtReturn;
        }

        public async Task<ResponseModel> APICallGetMethod(string url)
        {
            ResponseModel res = new ResponseModel();
            string uri = "api/" + url;
            string json = "";

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.GetAsync(uri);
                json = await result.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ResponseModel>(json);

            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public async Task<ResponseModel> APICallPostMethod(string url, string jsonPassIn)
        {
            ResponseModel res = new ResponseModel();
            string uri = "api/" + url;
            string json = "";
            var stringContent = new StringContent(jsonPassIn, UnicodeEncoding.UTF8, "application/json");

            try
            {
                HttpResponseMessage result = await InitApi.apiClient.PostAsync(uri, stringContent);
                json = await result.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ResponseModel>(json);
            }
            catch (Exception ex)
            {

            }

            return res;

        }

        public static string GetPage(string url)
        {
            string strRetrn = "";
            try
            {
                url = "http://localhost:60483/api/" + url;
                Uri ourUri = new Uri(url);
                // Creates an HttpWebRequest for the specified URL.
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(ourUri);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                // Releases the resources of the response.
                myHttpWebResponse.Close();
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        string challenge = null;
                        challenge = response.GetResponseHeader("WWW-Authenticate");
                        if (challenge != null)
                            //Console.WriteLine("\nThe following challenge was raised by the server:{0}", challenge);
                            strRetrn = "Authorization Failed.";
                    }
                    else
                    {
                        //Console.WriteLine("\nThe following WebException was raised : {0}", e.Message);
                        strRetrn = e.Message.ToString();
                    }
                }
                else
                {
                    //Console.WriteLine("\nResponse Received from server was null");
                    strRetrn = "No response from server.";
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("\nThe following Exception was raised : {0}", e.Message);
                strRetrn = e.Message.ToString();
            }

            return strRetrn;
        }
    }
}
