﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class ConstantVar
    {
        #region pageMode 1:New/Add 2:Edit 3:View
        public static int PageModeAdd = 1;
        public static int PageModeEdit = 2;
        public static int PageModeView = 3;
        #endregion
    }
}
