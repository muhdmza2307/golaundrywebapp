﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class APIUrlCallVar
    {
        #region Login/Logout
        public static string api_Login = "Account/login";
        #endregion

        #region Menu/Permission
        public static string api_MenuPerm = "User/GetUserMenuPermission?Id=$";
        #endregion


        #region Company
        public static string api_CompanyGridList = "Company/GetCompanyList";
        public static string api_ViewEditCompany = "Company/GetCompanyById?Id=$";
        #endregion

        #region User
        public static string api_UserGridList = "User/GetUserList";
        public static string api_ViewEditUser = "Company/GetCompanyById?Id=$";
        #endregion
    }
}
