﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class ListGridView
    {
        public List<GridView> GridView { get; set; } = new List<GridView>();

        public int GridviewNo { get; set; } = 0;

    }
}
