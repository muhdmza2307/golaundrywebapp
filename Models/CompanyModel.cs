using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace goLaundryWebApp.Models
{
    public class CompanyModel
    {
        public int? compId { get; set; }

        public string compName { get; set; }

        public string branchName { get; set; }

        public bool isParent { get; set; }

        public int? pCompId { get; set; }

        public string email { get; set; }

        public string telNo { get; set; }

        public string faxNo { get; set; }

        public string add1 { get; set; }

        public string add2 { get; set; }

        public string add3 { get; set; }

        public bool isActive { get; set; }

        public string strIsActive { get; set; }

    }
}
