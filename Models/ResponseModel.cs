﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class ResponseModel
    {
        public int type { get; set; }

        public string data { get; set; }

        public bool show { get; set; }

        public void setResponse(int typeValue, string dataValue, bool showValue)
        {
            type = typeValue;
            data = dataValue;
            show = showValue;
        }

    }
}
