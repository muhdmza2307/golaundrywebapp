﻿using goLaundryWebApp.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class sessionMenuPerm
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        Session clsSession = null;

        public sessionMenuPerm(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            clsSession = new Session(_httpContextAccessor);
        }

        public string GetMenu()
        {
            if (_session != null)
            {
                return clsSession.getSession("sessionUserMenuPerm");
            }
            else
                return "";
        }
    }
}
