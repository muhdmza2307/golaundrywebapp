﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace goLaundryWebApp.Models
{
    public class GridView
    {
        public GridView(string ID)
        {
            GridViewID = ID;
        }      
        public string GridViewID { get; set; } = "";

        #region ColumnSetting
        public int[] ColumnHide { get; set; } = new int[] { };
        public string IsCheckBoxRequired { get; set; } = "N";
        public string[] ColumnWidth { get; set; } = new string[] { };
        public int[] ColumnNoSort { get; set; } = new int[] { };
        #endregion

        #region Module
        public string GridViewModuleName { get; set; } = "";
        public string GridViewSubModuleName { get; set; } = "";
        #endregion

        #region DataSource
        public string GridViewApiCall { get; set; } = "";
        public DataTable dtGridView { get; set; } = new DataTable();
        #endregion

        #region Action
        public string IsActionRequired { get; set; } = "Y";
        public string actionSetup { get; set; } = "";
        #endregion 

        #region Paging
        public int ConstBreak { get; set; } = 10;
        public int Pagesize { get; set; } = 10;
        public int newI { get; set; } = 1;
        public int ConstRecordNo { get; set; } = 100;
        public string RightArrow { get; set; } = "";
        public string LeftArrow { get; set; } = "";
        public int CurPage { get; set; } = 1;
        public string ControllerFunc { get; set; } = "GridView/MultiGetList";
        public string ControllerFuncRight { get; set; } = "GridView/MultiRightArrowClick";
        public string ControllerFuncLeft { get; set; } = "GridView/MultiLeftArrowClick";
        public string rowFrom { get; set; } = "1";
        public string rowTo { get; set; } = "10";
        #endregion
    }
}
